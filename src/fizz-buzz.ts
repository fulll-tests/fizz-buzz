export class FizzBuzz {
  execute(end: number): string {
    if (end === 0) throw new Error("end must be > 0");

    if (end === 1) return "1";

    return this.execute(end - 1) + this.transformNumberIfNecessary(end);
  }

  // PRIVATE METHODS

  private transformNumberIfNecessary(num: number): string {
    if (this.canBeDividedBy3And5(num)) return "FizzBuzz";

    if (this.canBeDividedBy3(num)) return "Fizz";

    if (this.canBeDividedBy5(num)) return "Buzz";

    return num.toString();
  }

  private canBeDividedBy3And5(num: number): boolean {
    return this.canBeDividedBy3(num) && this.canBeDividedBy5(num);
  }

  private canBeDividedBy5(num: number): boolean {
    return num % 5 === 0;
  }

  private canBeDividedBy3(num: number): boolean {
    return num % 3 === 0;
  }
}
