import { FizzBuzz } from "./fizz-buzz";

const SUT = (end: number): string => {
  const fizzbuzz = new FizzBuzz();
  return fizzbuzz.execute(end);
};

describe("execute", () => {
  it("should display 1 when end is 1", async () => {
    // Arrange

    // Act
    const result = SUT(1);

    // Assert
    expect(result).toEqual("1");
  });

  it("should display 12 when end is 2", async () => {
    // Arrange

    // Act
    const result = SUT(2);

    // Assert
    expect(result).toEqual("12");
  });

  it("should display 12Fizz when end is 3", async () => {
    // Arrange

    // Act
    const result = SUT(3);

    // Assert
    expect(result).toEqual("12Fizz");
  });

  it("should display 12Fizz4 when end is 4", async () => {
    // Arrange

    // Act
    const result = SUT(4);

    // Assert
    expect(result).toEqual("12Fizz4");
  });

  it("should display 12Fizz4Buzz when end is 5", async () => {
    // Arrange

    // Act
    const result = SUT(5);

    // Assert
    expect(result).toEqual("12Fizz4Buzz");
  });

  it("should display 12Fizz4BuzzFizz when end is 6", async () => {
    // Arrange

    // Act
    const result = SUT(6);

    // Assert
    expect(result).toEqual("12Fizz4BuzzFizz");
  });

  it("should display 12Fizz4BuzzFizz78FizzBuzz when end is 10", async () => {
    // Arrange

    // Act
    const result = SUT(10);

    // Assert
    expect(result).toEqual("12Fizz4BuzzFizz78FizzBuzz");
  });

  it("should display 12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz when end is 15", async () => {
    // Arrange

    // Act
    const result = SUT(15);

    // Assert
    expect(result).toEqual("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz");
  });

  it("should throw error when end is 0", async () => {
    // Arrange

    try {
      // Act
      SUT(0);
    } catch (e) {
      // Assert
      expect(e).toStrictEqual(new Error("end must be > 0"));
    }
    expect.assertions(1);
  });
});
