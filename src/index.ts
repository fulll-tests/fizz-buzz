import { FizzBuzz } from "./fizz-buzz";

const invalidArgumentError = (): Error =>
  new Error("You must specify a number as argument");

const getEndNumberArgument = (): number => {
  const arg = process.argv[2];

  if (!arg) throw invalidArgumentError();

  const end = Number(arg);

  if (!end) throw invalidArgumentError();

  return end;
};

const handleFizzBuzz = (end: number): void => {
  const fizzBuzz = new FizzBuzz();
  const result = fizzBuzz.execute(end);
  console.log(result);
};

try {
  const end = getEndNumberArgument();
  handleFizzBuzz(end);
} catch (e) {
  console.log(e.message);
}
